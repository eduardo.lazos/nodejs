import express from "express";

const app = express();

const port = 8080;

/**
 * - req
 * - res
 *
 * - middlewares
 * - log
 * - morgan
 */

function logger(req, res, next) {
  const { method, path, url, cookies, body, xhr, params, query } = req;

  console.log({
    method,
    path,
    url,
    cookies,
    body,
    xhr,
    params,
    query,
  });

  next();
}

app.use(logger)

app.all("/users", (req, res) => {
  res.status(203).json({
    foo: "bar",
  });
});

app.all("/users/:id", (req, res) => {
  res.status(203).json({
    foo: "bar",
  });
});

app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
